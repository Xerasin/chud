local LocalPlayer = LocalPlayer
local SysTime = SysTime
local RealFrameTime = RealFrameTime
local Lerp = Lerp
local IsValid = IsValid

local mathClamp = math.Clamp
local mathPow = math.pow
local mathMin = math.min
local mathMax = math.max
local mathAbs = math.abs
local mathSin = math.sin
local mathFloor = math.floor

local surfacePlaySound = surface.PlaySound
local surfaceSetAlphaMultiplier = surface.SetAlphaMultiplier

local drawDrawText = draw.DrawText

local renderSetScissorRect = render.SetScissorRect

local GTSC = CHUD.Utils.GetTextSize

local lply = LocalPlayer()

local qi_ammoFade      = 0
local qi_healthFade    = 0
local qi_lastAmmo      = 0
local qi_lastHealth    = 100
local qi_warnAmmo      = false
local qi_warnHealth    = false
local qi_fadedOut      = false
local qi_dimmed        = false
local qi_lastEventTime = 0
local qi_alpha         = 255

local alpha = 255

local QUICKINFO_EVENT_DURATION  = 1.0
local QUICKINFO_BRIGHTNESS_FULL = 255
local QUICKINFO_BRIGHTNESS_DIM  = 64
local QUICKINFO_FADE_IN_TIME    = 0.5
local QUICKINFO_FADE_OUT_TIME   = 2

local chud_quickinfo_snd = CHUD.CVars.QuickInfoSounds

local function DrawWarning(x, y, char, time)
    local scale = mathAbs(mathSin(SysTime()*8)) * 128

    if time <= (RealFrameTime() * 200) then
        if scale < 40 then
            time = 0
            return time
        else
            time = time + (RealFrameTime() * 200)
        end
    end

    time = time - (RealFrameTime() * 200)
    local caution = CHUD.Utils.GetDamageColor()
    caution.a = scale

    drawDrawText(char, "CHUD-HL2-QuickInfo", x, y, caution)

    return time
end

local function DrawIconProgressBar(x, y, char, char2, percentage, color)
    percentage = mathMin(1, percentage)
    percentage = mathMax(0, percentage)

    local w, h = GTSC("CHUD-HL2-QuickInfo",char)
    h = h + 1

    local barOfs = h * percentage

    renderSetScissorRect(x, y, x + w, y + barOfs, true)
    drawDrawText(char2, "CHUD-HL2-QuickInfo", x, y, color)
    renderSetScissorRect(0, 0, 0, 0, false)

    renderSetScissorRect(x, y + barOfs, x + w, y + h, true)
    drawDrawText(char, "CHUD-HL2-QuickInfo", x, y, color)
    renderSetScissorRect(0, 0, 0, 0, false)
end

local function DrawQuickInfo(x, y)
    if not IsValid(lply) then lply = LocalPlayer() return end
    local wep = lply:GetActiveWeapon()
    if not IsValid(wep) then return end
    if not lply:Alive() then return end
    if lply:ShouldDrawLocalPlayer() then return end

    local fadeOut = lply:KeyDown(IN_ZOOM)

    local IsTFA = IsValid(wep) and wep.IsTFA and wep:IsTFA() or wep.IsTFAWeapon

    if qi_fadedOut ~= fadeOut then
        qi_dimmed = false

        if fadeOut then
            qi_alpha = Lerp(RealFrameTime() / 0.25, qi_alpha, 0)
            if mathFloor(qi_alpha + 0.5) == 0 then
                qi_fadedOut = fadeOut
            end
        else
            qi_alpha = Lerp(RealFrameTime() / QUICKINFO_FADE_IN_TIME, qi_alpha, QUICKINFO_BRIGHTNESS_FULL)
            if mathFloor(qi_alpha + 0.5) == QUICKINFO_BRIGHTNESS_FULL then
                qi_fadedOut = fadeOut
            end
        end
    elseif not qi_fadedOut then
        if (SysTime() - qi_lastEventTime) > QUICKINFO_EVENT_DURATION then
            if not qi_dimmed then
                qi_alpha = Lerp(RealFrameTime() / QUICKINFO_FADE_OUT_TIME, qi_alpha, QUICKINFO_BRIGHTNESS_DIM)
                if mathFloor(qi_alpha + 0.5) == QUICKINFO_BRIGHTNESS_DIM then
                    qi_dimmed = true
                end
            end
        elseif qi_dimmed then
            qi_alpha = Lerp(RealFrameTime() / QUICKINFO_FADE_IN_TIME, qi_alpha, QUICKINFO_BRIGHTNESS_FULL)
            if mathFloor(qi_alpha + 0.5) == QUICKINFO_BRIGHTNESS_FULL then
                qi_dimmed = false
            end
        end
    end

    local scalar = 138 / 255
    if IsTFA then
        alpha = qi_alpha * mathPow(mathMin(1 - ((wep.IronSightsProgress and not wep.DrawCrosshairIS) and wep.IronSightsProgress or 0), 1 - (wep.SprintProgress or 0), 1 - (wep.InspectingProgress or 0), wep.clrelp or 0), 2)
    else
        alpha = qi_alpha
    end

    surfaceSetAlphaMultiplier(alpha / 255)

    local health = lply:Health()
    if health ~= qi_lastHealth then
        qi_alpha = 255
        qi_lastEventTime = SysTime()
        qi_lastHealth = health

        local healthPerc = health / lply:GetMaxHealth()

        if healthPerc <= 0.25 then
            if qi_warnHealth == false then
                qi_healthFade = 255
                qi_warnHealth = true

                if chud_quickinfo_snd:GetBool() then
                    surfacePlaySound("common/warning.wav")
                end
            end
        else
            qi_warnHealth = false
        end
    end

    local ammo = wep:Clip1()
    if ammo ~= qi_lastAmmo then
        qi_alpha = 255
        qi_lastEventTime = SysTime()
        qi_lastAmmo = ammo

        local ammoPerc = ammo / wep:GetMaxClip1()

        if wep:GetMaxClip1() > 1 and ammoPerc <= 0.25 then
            if qi_warnAmmo == false then
                qi_ammoFade = 255
                qi_warnAmmo = true

                if chud_quickinfo_snd:GetBool() then
                    surfacePlaySound("common/warning.wav")
                end
            end
        else
            qi_warnAmmo = false
        end
    end

    local sinScale = mathAbs(mathSin(SysTime() * 8)) * 128

    if qi_healthFade > 0 then
        local w,h = GTSC("CHUD-HL2-QuickInfo","[")
        qi_healthFade = DrawWarning(x - (w * 2), y - h / 2, "[", qi_healthFade)
    else
        local healthPerc = mathClamp(health / lply:GetMaxHealth(), 0, 1)
        local healthColor = qi_warnHealth and CHUD.Utils.GetDamageColor() or CHUD.Utils.GetHUDColor()

        if qi_warnHealth then
            healthColor.a = sinScale
        else
            healthColor.a = scalar * 255
        end

        local w,h = GTSC("CHUD-HL2-QuickInfo","[")
        DrawIconProgressBar(x - (w * 2), y - h / 2, "[", "{", 1 - healthPerc, healthColor)
    end

    if qi_ammoFade > 0 then
        local w,h = GTSC("CHUD-HL2-QuickInfo","]")
        qi_ammoFade = DrawWarning(x + w, y - h / 2, "]", qi_ammoFade)
    else
        local ammoPerc

        if wep:GetMaxClip1() <= 0 then
            ammoPerc = 0
        else
            ammoPerc = mathClamp(1 - (ammo / wep:GetMaxClip1()), 0, 1)
        end

        local ammoColor = qi_warnAmmo and CHUD.Utils.GetDamageColor() or CHUD.Utils.GetHUDColor()

        if qi_warnAmmo then
            ammoColor.a = sinScale
        else
            ammoColor.a = scalar * 255
        end

        local w,h = GTSC("CHUD-HL2-QuickInfo","]")
        DrawIconProgressBar(x + w, y - h / 2, "]", "}", ammoPerc, ammoColor)
    end

    surfaceSetAlphaMultiplier(1)
end

local drawQI = true
local enabled = CHUD.CVars.Enabled
hook.Add("HUDShouldDraw", "CHUD.QuickInfo", function(name)
    if name == "CHUDQuickInfo" then
        if not enabled:GetBool() then return true end
        if IsValid(wep) and wep:IsWeapon() then
            if isfunction(wep.DoDrawCrosshair) then
                local IsTFA = IsValid(wep) and wep.IsTFA and wep:IsTFA() or wep.IsTFAWeapon
                if xhair_oveerride:GetBool() then
                    drawQI = true
                elseif IsTFA and xhair_tfa:GetBool() then
                    drawQI = true
                else
                    drawQI = true
                end
            end

            if wep.crosshairVisible and isfunction(wep.crosshairVisible) then
                drawQI = false
            else
                drawQI = true
            end

            if wep.ShouldDrawCrosshair then
                drawQI = wep:ShouldDrawCrosshair() or true
            end
        end
        return false
    end
end)

local chud_quickinfo = CHUD.CVars.QuickInfo
local cl_drawhud = GetConVar("cl_drawhud")
hook.Add("HUDPaint", "CHUD.QuickInfo", function()
    if not IsValid(lply) then lply = LocalPlayer() end
    if not cl_drawhud:GetBool() then return end
    if not enabled:GetBool() then return end
    if drawQI and chud_quickinfo:GetBool() then
        DrawQuickInfo(CHUD.CurResW / 2, CHUD.CurResH / 2)
    end
end)