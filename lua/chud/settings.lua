local draw,surface,math,string,LocalPlayer,ScreenScaleH = draw,surface,math,string,LocalPlayer,CHUD.Utils.ScreenScaleH
local GTSC = CHUD.Utils.GetTextSize

local function Main(panel)
    local conf = {
        Options = {},
        CVars = {},
        Label = "#Presets",
        MenuButton = "1",
        Folder = "chud_main"
    }

    conf.Options["#Default"] = {
        chud_enabled        = "1",
        chud_wpn_enabled    = "1",
        chud_showvelocity   = "0",
        chud_disable_extras = "1",
        chud_hidehpatfull   = "1",
        chud_negativehp     = "0",
        chud_altammo_oldpos = "1",
        chud_additivefonts  = "1",
    }

    panel:AddControl("ComboBox", conf)

    local lbl = vgui.Create("DLabel",panel)
    lbl:Dock(TOP)
    lbl:SetFont("DermaLarge")
    lbl:SetText("Main")
    lbl:SizeToContents()
    lbl:DockMargin(8,16,8,0)
    lbl:SetDark(true)

    panel:CheckBox("Enable CHUD","chud_enabled")

    panel:CheckBox("Enable Custom Weapon Switcher","chud_wpn_enabled")

    local lbl = vgui.Create("DLabel",panel)
    lbl:Dock(TOP)
    lbl:SetFont("DermaLarge")
    lbl:SetText("Features")
    lbl:SizeToContents()
    lbl:DockMargin(8,16,8,0)
    lbl:SetDark(true)

    panel:CheckBox("Show Velocity","chud_showvelocity")

    panel:CheckBox("Disable extra, unused/annoying HUD elements","chud_disable_extras")
    panel:ControlHelp("Disables zoom overlay, squad status, poison indicator, geiger clicking sound and side damage indicators")

    panel:CheckBox("Hide Health at full HP","chud_hidehpatfull")

    panel:CheckBox("Show Negative HP Values","chud_negativehp")

    panel:CheckBox("Use Stock Secondary Ammo Position","chud_altammo_oldpos")
    panel:ControlHelp("Disabled: Secondary Ammo displayed above Primary Ammo")

    panel:CheckBox("Additive Fonts","chud_additivefonts")
    panel:ControlHelp("Disabled: Fonts become solid apposed to transparent. Accuracy lost in return for visibility.")
end

local function Colors(panel)
    local conf = {
        Options = {},
        CVars = {},
        Label = "#Presets",
        MenuButton = "1",
        Folder = "chud_colors"
    }

    conf.Options["#Default"] = {
        chud_customcolor     = "255 220 0",
        chud_damagecolor     = "255 0 0",
        chud_useplayercolor  = "0",
        chud_useweaponcolor  = "0",
        chud_useteamcolor    = "0",

        chud_backgroundcolor   = "0 0 0",
        chud_backgroundalpha   = "80",
        chud_useplayercolor_bg = "0",
        chud_useweaponcolor_bg = "0",
        chud_useteamcolor_bg   = "0",

        chud_rainbow    = "0",
        chud_rbow_speed = "2",
        chud_rbow_sat   = "1",
        chud_rbow_value = "1",

        chud_rainbow_bg    = "0",
        chud_rbow_speed_bg = "2",
        chud_rbow_sat_bg   = "1",
        chud_rbow_value_bg = "1",
    }

    panel:AddControl("ComboBox", conf)

    local preview = vgui.Create("EditablePanel",panel)
    preview:Dock(TOP)
    preview:SetTall(ScreenScaleH(20)+64)
    preview:DockMargin(8,8,8,8)
    local x = 0
    function preview:Paint(w,h)
        x = x+0.5
        draw.RoundedBox(0,x,0,w,h,Color(0,0,0))
        draw.RoundedBox(0,x-w,0,w,h,Color(255,255,255))
        draw.RoundedBox(0,x-(w*2),0,w,h,Color(0,0,0))
        local style = CHUD.Styles[CHUD.CVars.Style:GetString()]

        if not style or not style.Draw then
            style = CHUD.Styles["hl2"]
        end

        if not CHUD.Styles["hl2"] then
            return
        end

        style.Demo(100,16,24)
        if x > w*2 then x = 0 end
    end

    local preview2 = vgui.Create("EditablePanel",panel)
    preview2:Dock(TOP)
    preview2:SetTall(ScreenScaleH(20)+64)
    preview2:DockMargin(8,8,8,8)
    local x = 0
    function preview2:Paint(w,h)
        x = x+0.5
        draw.RoundedBox(0,x,0,w,h,Color(0,0,0))
        draw.RoundedBox(0,x-w,0,w,h,Color(255,255,255))
        draw.RoundedBox(0,x-(w*2),0,w,h,Color(0,0,0))
        local style = CHUD.Styles[CHUD.CVars.Style:GetString()]

        if not style or not style.Draw then
            style = CHUD.Styles["hl2"]
        end

        if not CHUD.Styles["hl2"] then
            return
        end

        style.Demo(25,16,24)
        if x > w*2 then x = 0 end
    end

    local stylePnl = vgui.Create("EditablePanel",panel)
    stylePnl:Dock(TOP)
    stylePnl:SetTall(24)
    stylePnl:DockMargin(8,8,8,8)
    local slbl = stylePnl:Add("DLabel")
    slbl:Dock(LEFT)
    slbl:DockMargin(4,4,36,4)
    slbl:SetText("HUD Style")
    slbl:SizeToContents()
    slbl:SetDark(true)
    local styles = stylePnl:Add("DComboBox")
    styles:Dock(FILL)
    styles:SetValue(CHUD.Styles[CHUD.CVars.Style:GetString()].Name or CHUD.Styles["hl2"].Name)
    for id,data in pairs(CHUD.Styles) do
        if not data.Draw or not data.Demo or not isfunction(data.Draw) or not isfunction(data.Demo) then continue end
        styles:AddChoice(data.Name,id,id == CHUD.CVars.Style:GetString())
    end
    styles.OnSelect = function(s,i,name,value)
        CHUD.CVars.Style:SetString(value)
    end

    local wpnPnl = vgui.Create("EditablePanel",panel)
    wpnPnl:Dock(TOP)
    wpnPnl:SetTall(24)
    wpnPnl:DockMargin(8,8,8,8)
    local wlbl = wpnPnl:Add("DLabel")
    wlbl:Dock(LEFT)
    wlbl:DockMargin(4,4,36,4)
    wlbl:SetText("Weapon Switcher")
    wlbl:SizeToContents()
    wlbl:SetDark(true)
    local wpnStyle = wpnPnl:Add("DComboBox")
    wpnStyle:Dock(FILL)
    wpnStyle:SetValue(CHUD.Styles[CHUD.CVars.StyleWeapon:GetString()].Name or CHUD.Styles["hl2"].Name)
    for id,data in pairs(CHUD.Styles) do
        if not data.HasWeaponSwitcher then continue end
        wpnStyle:AddChoice(data.Name,id,id == CHUD.CVars.Style:GetString())
    end
    wpnStyle.OnSelect = function(s,i,name,value)
        CHUD.CVars.StyleWeapon:SetString(value)
    end

    panel:Help("\nForeground/Text Color:")

    local mixer1 = vgui.Create("DColorMixer",panel)
    mixer1:Dock(TOP)
    mixer1:SetTall(245)
    mixer1:DockMargin(8,8,8,8)
    mixer1:SetAlphaBar(false)
    mixer1:SetColor(CHUD.Utils.GetHUDColor())
    function mixer1:ValueChanged()
        local color = self:GetColor()
        RunConsoleCommand("chud_customcolor",("%d %d %d"):format(color.r,color.g,color.b))
    end

    panel:CheckBox("Use Player Color","chud_useplayercolor")
    panel:CheckBox("Use Weapon Color","chud_useweaponcolor")
    panel:CheckBox("Use Team Color","chud_useteamcolor")

    panel:Help("\nDamaged/Low Ammo Color:")

    local mixer2 = vgui.Create("DColorMixer",panel)
    mixer2:Dock(TOP)
    mixer2:SetTall(245)
    mixer2:DockMargin(8,8,8,8)
    mixer2:SetAlphaBar(false)
    mixer2:SetColor(CHUD.Utils.GetDamageColor())
    function mixer2:ValueChanged()
        local color = self:GetColor()
        RunConsoleCommand("chud_damagecolor",("%d %d %d"):format(color.r,color.g,color.b))
    end

    panel:CheckBox("Use Player Color","chud_useplayercolor_dmg")
    panel:CheckBox("Use Weapon Color","chud_useweaponcolor_dmg")
    panel:CheckBox("Use Team Color","chud_useteamcolor_dmg")

    panel:Help("\nBackground Color:")

    local mixer3 = vgui.Create("DColorMixer",panel)
    mixer3:Dock(TOP)
    mixer3:SetTall(245)
    mixer3:DockMargin(8,8,8,8)
    mixer3:SetAlphaBar(false)
    mixer3:SetColor(CHUD.Utils.GetBGColor())
    function mixer3:ValueChanged()
        local color = self:GetColor()
        RunConsoleCommand("chud_backgroundcolor",("%d %d %d"):format(color.r,color.g,color.b))
    end

    panel:NumSlider("Alpha","chud_backgroundalpha",0,255,0)
    panel:CheckBox("Use Player Color","chud_useplayercolor_bg")
    panel:CheckBox("Use Weapon Color","chud_useweaponcolor_bg")
    panel:CheckBox("Use Team Color","chud_useteamcolor_bg")

    panel:Help("\nRainbow Settings:")

    panel:CheckBox("Enable for Foreground","chud_rainbow")
    panel:NumSlider("FG: Speed","chud_rbow_speed",1,1000,0)
    panel:NumSlider("FG: Saturation","chud_rbow_sat",0,1,2)
    panel:NumSlider("FG: Value","chud_rbow_value",0,1,2)
    panel:CheckBox("Enable for Background","chud_rainbow_bg")
    panel:NumSlider("BG: Speed","chud_rbow_speed_bg",1,1000,0)
    panel:NumSlider("BG: Saturation","chud_rbow_sat_bg",0,1,2)
    panel:NumSlider("BG: Value","chud_rbow_value_bg",0,1,2)
end

-- Crosshair --

local function DrawDynamicCrosshair(x,y)
    local size = CHUD.CVars.CrosshairLength:GetInt()
    local width = CHUD.CVars.CrosshairSize:GetInt()
    local space = CHUD.CVars.CrosshairSpace:GetInt()
    local color = CHUD.CVars.CrosshairUseHUD:GetBool() and CHUD.Utils.GetHUDColor() or CHUD.Utils.GetCrosshairColor()
    local alpha = CHUD.CVars.CrosshairAlpha:GetInt()
    local dynamic = CHUD.CVars.CrosshairDynamic:GetBool()
    local outline = CHUD.CVars.CrosshairOutline:GetBool()

    local movementRecoil = 0
    if dynamic and IsValid(ply) then
        movementRecoil = ply.MovementRecoil or 0
        local gap = 15 * movementRecoil
        space = space + gap
    end

    local sizeH = size / 2
    local widthH = width / 2

    if outline then
        surface.SetDrawColor(0,0,0,alpha)
        surface.DrawOutlinedRect(x - (width / 2) - 1, y - sizeH - space - 1, width + 2, sizeH + 2) --top
        surface.DrawOutlinedRect(x - sizeH - space - 1, y - (width / 2) - 1, sizeH + 2, width + 2) --left
        surface.DrawOutlinedRect(x - (width / 2) - 1, y + space - 1, width + 2, sizeH + 2) --bottom
        surface.DrawOutlinedRect(x + space - 1, y - (width / 2) - 1, sizeH + 2, width + 2) --right
    end

    surface.SetDrawColor(ColorAlpha(color,alpha))
    surface.DrawRect(x - (width / 2), y - sizeH - space, width, sizeH) --top
    surface.DrawRect(x - sizeH - space, y - (width / 2), sizeH, width) --left
    surface.DrawRect(x - (width / 2), y + space, width, sizeH) --bottom
    surface.DrawRect(x + space, y - (width / 2), sizeH, width) --right
end

local x1 = Material("sprites/hud/v_crosshair1")
local x2 = Material("sprites/hud/v_crosshair2")
local xw1,xh1 = x1:Width(),x1:Height()
local xw2,xh2 = x2:Width(),x2:Height()

local circle
local shadowCircle
local function DrawCrosshair(x,y)
    local style = CHUD.CVars.CrosshairStyle:GetInt()
    local color = CHUD.CVars.CrosshairUseHUD:GetBool() and CHUD.Utils.GetHUDColor() or CHUD.Utils.GetCrosshairColor()

    if style == -1 then
        --draw nothing lol
    elseif style == 1 then
        draw.NoTexture()
        surface.SetDrawColor(Color(0,0,0,192))
        shadowCircle = CHUD.Utils.DrawCircle(x, y, 7, 8, shadowCircle)

        surface.SetDrawColor(color)
        circle = CHUD.Utils.DrawCircle(x, y, 5, 8, circle)
    elseif style == 2 then
        surface.SetDrawColor(color)
        surface.SetMaterial(x1)
        surface.DrawTexturedRect(x-xw1/2, y-xh1/2, xw1, xh1)
    elseif style == 3 then
        surface.SetDrawColor(color)
        surface.SetMaterial(x2)
        surface.DrawTexturedRect(x-xw2/2, y-xh2/2, xw2, xh2)
    elseif style == 4 then
        DrawDynamicCrosshair(x,y)
    else
        local w,h = GTSC("CHUD-HL2-Crosshairs","Q")
        draw.DrawText("Q", "CHUD-HL2-Crosshairs", x, y-h/2, color, TEXT_ALIGN_CENTER)
    end
end

-- Subtract 2
local crosshairs = {
    [1] = "Disabled",
    [2] = "HL2 Default",
    [3] = "Dot",
    [4] = "Vehicle",
    [5] = "Vehicle Alt",
    [6] = "Dynamic",
}
local function Crosshair(panel)
    local conf = {
        Options = {},
        CVars = {},
        Label = "#Presets",
        MenuButton = "1",
        Folder = "chud_crosshair"
    }

    conf.Options["#Default"] = {
        chud_crosshair         = "0",
        chud_crosshair_size    = "2",
        chud_crosshair_length  = "8",
        chud_crosshair_space   = "4",
        chud_crosshair_outline = "1",
        chud_crosshair_dynamic = "1",
        chud_crosshair_color   = "255 255 255",
        chud_crosshair_alpha   = "255",
        chud_crosshair_usehud  = "1",
    }

    panel:AddControl("ComboBox", conf)

    local preview = vgui.Create("EditablePanel",panel)
    preview:Dock(TOP)
    preview:SetTall(ScreenScaleH(20)+64)
    preview:DockMargin(8,8,8,8)
    local x = 0
    function preview:Paint(w,h)
        x = x+0.5
        draw.RoundedBox(0,x,0,w,h,Color(0,0,0))
        draw.RoundedBox(0,x-w,0,w,h,Color(255,255,255))
        draw.RoundedBox(0,x-(w*2),0,w,h,Color(0,0,0))
        DrawCrosshair(w/2,h/2)
        if x > w*2 then x = 0 end
    end

    local comboPnl = vgui.Create("EditablePanel",panel)
    comboPnl:Dock(TOP)
    comboPnl:SetTall(24)
    comboPnl:DockMargin(8,8,8,8)
    local clabel = comboPnl:Add("DLabel")
    clabel:Dock(LEFT)
    clabel:DockMargin(4,4,36,4)
    clabel:SetText("Crosshair Style")
    clabel:SizeToContents()
    clabel:SetDark(true)
    local combo = comboPnl:Add("DComboBox")
    combo:Dock(FILL)
    combo:SetValue(crosshairs[CHUD.CVars.CrosshairStyle:GetInt()-2] or crosshairs[1])
    for value,name in pairs(crosshairs) do
        combo:AddChoice(name,value,CHUD.CVars.CrosshairStyle:GetInt() == value-2)
    end
    combo.OnSelect = function(s,i,name,value)
        CHUD.CVars.CrosshairStyle:SetInt(value-2)
    end

    panel:Help("Crosshair Color:")

    local mixer = vgui.Create("DColorMixer",panel)
    mixer:Dock(TOP)
    mixer:SetTall(245)
    mixer:DockMargin(8,8,8,8)
    mixer:SetAlphaBar(false)
    mixer:SetColor(CHUD.Utils.GetCrosshairColor())
    function mixer:ValueChanged()
        local color = self:GetColor()
        RunConsoleCommand("chud_crosshair_color",("%d %d %d"):format(color.r,color.g,color.b))
    end

    panel:NumSlider("Alpha","chud_crosshair_alpha",0,255,0)
    panel:CheckBox("Use HUD Color","chud_crosshair_usehud")

    panel:Help("\nDynamic Crosshair Settings:")

    panel:CheckBox("Movement Based Spacing","chud_crosshair_dynamic")
    panel:CheckBox("Outline","chud_crosshair_outline")
    panel:NumSlider("Length","chud_crosshair_length",0,128,0)
    panel:NumSlider("Width","chud_crosshair_size",0,64,0)
    panel:NumSlider("Spacing","chud_crosshair_space",0,24,0)
end

hook.Add("PopulateToolMenu","CHUD",function()
	spawnmenu.AddToolMenuOption("Utilities","CHUD","CHUD1","Configuration","","",Main)
	spawnmenu.AddToolMenuOption("Utilities","CHUD","CHUD2","Colors & Style","","",Colors)
    spawnmenu.AddToolMenuOption("Utilities","CHUD","CHUD3","Crosshair","","",Crosshair)
end)