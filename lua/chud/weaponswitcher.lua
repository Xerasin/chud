--based off of goldsrc hud's logic
--but i cleaned it up a lot and made it actually work
--and compatability for other addons
--(wire kinda buggy nowadays, havent retested submaterial tool)

--other than skidding goldsrc hud's logic (which IS MIT licensed so)
--everything else is hand done
local sndMove = Sound("Player.WeaponSelectionMoveSlot")
local sndSelect = Sound("Player.WeaponSelectionClose")

local LocalPlayer = LocalPlayer
local CurTime = CurTime
local SysTime = SysTime
local RealFrameTime = RealFrameTime
local pairs = pairs
local Material = Material
local IsValid = IsValid
local Lerp = Lerp
local language_GetPhrase = language.GetPhrase
local draw_DrawText = draw.DrawText
local draw_RoundedBox = draw.RoundedBox
local surface_SetDrawColor = surface.SetDrawColor
local surface_SetTexture = surface.SetTexture
local surface_DrawTexturedRect = surface.DrawTexturedRect
local surface_GetTextureID = surface.GetTextureID
local table_sort = table.sort
local table_Count = table.Count
local input_SelectWeapon = input.SelectWeapon
local input_IsKeyDown = input.IsKeyDown

local ScreenScaleH = CHUD.Utils.ScreenScaleH
local GTSC = CHUD.Utils.GetTextSize

local lply = LocalPlayer()

local physgunPickup = false
local lastPhysgunBeam = CurTime()
hook.Add("DrawPhysgunBeam", "CHUD.TotallyDidntStealThisFromGoldsrcHUD", function(ply, physgun, enabled, target, bone, hitPos)
    if not IsValid(lply) then lply = LocalPlayer() end
    if ply == lply and IsValid(target) then
        physgunPickup = true
        lastPhysgunBeam = CurTime()
    end
end)
hook.Add("Think", "CHUD.TotallyDidntStealThisFromGoldsrcHUD", function()
    if lastPhysgunBeam < CurTime() and physgunPickup then
        physgunPickup = false
    end
end)

local curSlot = 0
local curPos = 1
local nextCache = 0
local selTime = 0
local wepCount = 0

local tCache = {}
local tCacheLength = {}

--Drawing

local WeaponNames = {
    weapon_smg1          = language_GetPhrase("#HL2_SMG1"),
    weapon_shotgun       = language_GetPhrase("#HL2_Shotgun"),
    weapon_crowbar       = language_GetPhrase("#HL2_Crowbar"),
    weapon_pistol        = language_GetPhrase("#HL2_Pistol"),
    weapon_357           = language_GetPhrase("#HL2_357Handgun"),
    weapon_crossbow      = language_GetPhrase("#HL2_Crossbow"),
    weapon_physgun       = language_GetPhrase("#GMOD_Physgun"),
    weapon_rpg           = language_GetPhrase("#HL2_RPG"),
    weapon_bugbait       = language_GetPhrase("#HL2_Bugbait"),
    weapon_frag          = language_GetPhrase("#HL2_Grenade"),
    weapon_ar2           = language_GetPhrase("#HL2_Pulse_Rifle"),
    weapon_physcannon    = language_GetPhrase("#HL2_GravityGun"),
    weapon_stunstick     = language_GetPhrase("#HL2_StunBaton"),
    weapon_slam          = language_GetPhrase("#HL2_SLAM"),
    weapon_annabelle     = language_GetPhrase("#HL2_Annabelle"),
    weapon_handgrenade   = language_GetPhrase("#HL1_HandGrenade"),
    gmod_camera          = language_GetPhrase("#GMOD_Camera"),
    weapon_cubemap       = "Cubemap",
    none                 = "Hands",
}

local WeaponIconsHL2 = {
    weapon_smg1        = "a",
    weapon_shotgun     = "b",
    weapon_crowbar     = "c",
    weapon_pistol      = "d",
    weapon_357         = "e",
    weapon_crossbow    = "g",
    weapon_physgun     = "h",
    weapon_rpg         = "i",
    weapon_bugbait     = "j",
    weapon_frag        = "k",
    weapon_ar2         = "l",
    weapon_physcannon  = "m",
    weapon_stunstick   = "n",
    weapon_slam        = "o",
    weapon_medkit      = "+",
    hands              = "C",
    none               = "C",
    weapon_slap        = "\xe2\x9a\xa1",
    weapon_annabelle   = "(",
    weapon_alyxgun     = "%",

    --novus's rewritten weapons
    weapon_smg1r      = "a",
    weapon_shotgunr   = "b",
    weapon_crowbarr   = "c",
    weapon_pistolr    = "d",
    weapon_357r       = "e",
    weapon_crossbowr  = "g",
    weapon_rpgr       = "i",
    weapon_bugbaitr   = "j",
    weapon_fragr      = "k",
    weapon_ar2r       = "l",
    weapon_stunstickr = "n",
    weapon_slamr      = "o",
    weapon_alyxgunr   = "%",
    weapon_annabeller = "(",

    --unobtanable stuff
    weapon_cubemap         = "",
    weapon_citizenpackage  = "",
    weapon_citizensuitcase = "",
    weapon_oldmanharpoon   = "",

    --hl1
    weapon_357_hl1      = "",
    weapon_crossbow_hl1 = "",
    weapon_crowbar_hl1  = "",
    weapon_egon         = "",
    weapon_gauss        = "h",
    weapon_glock_hl1    = "",
    weapon_handgrenade  = "",
    weapon_hornetgun    = "",
    weapon_mp5_hl1      = "",
    weapon_rpg_hl1      = "",
    weapon_satchel      = "",
    weapon_shotgun_hl1  = "",
    weapon_snark        = "",
    weapon_tripmine     = "",

    --misc
    weapon_cyn_rr      = "$",
    weapon_cyn_crowbar = "c",

    weapon_archerxbow = ")",
    weapon_coilgun    = ":",
    weapon_plasmanade = "_",
    weapon_scrapper   = "z",
    weapon_asmd       = "f",

    oc_grapple        = "!",

    --cw2.0 stray weapons
    cw_mr96 = "$",
}

local WeaponIconsCS = {
    weapon_ak47         = "b",
    weapon_glock        = "c",
    weapon_tmp          = "d",
    weapon_aug          = "e",
    weapon_deagle       = "f",
    weapon_flashbang    = "g",
    weapon_hegrenade    = "h",
    weapon_g3sg1        = "i",
    weapon_knife        = "j",
    weapon_m3           = "k",
    weapon_mac10        = "l",
    weapon_p90          = "m",
    weapon_scout        = "n",
    weapon_sg550        = "o",
    weapon_smokegrenade = "p",
    weapon_ump45        = "q",
    weapon_awp          = "r",
    weapon_elite        = "s",
    weapon_famas        = "t",
    weapon_usp          = "u",
    weapon_galil        = "v",
    weapon_m4a1         = "w",
    weapon_mp5navy      = "x",
    weapon_p228         = "y",
    weapon_m249         = "z",
    weapon_sg552        = "A",
    weapon_xm1014       = "B",
    weapon_c4           = "C",

    --misc
    weapon_cyn_xm1014 = "B",
    weapon_cyn_m3     = "k",
}

local WeaponIconsCSD = {
    cw_ak74                   = "b",
    cw_ar15                   = "w",
    cw_extrema_ratio_official = "j",
    cw_flash_grenade          = "P",
    cw_fiveseven              = "u",
    cw_scarh                  = "i",
    cw_frag_grenade           = "O",
    cw_g3a3                   = "i",
    cw_g36c                   = "i",
    cw_ump45                  = "q",
    cw_mp5                    = "x",
    cw_deagle                 = "f",
    cw_l115                   = "r",
    cw_l85a2                  = "i",
    cw_m14                    = "n",
    cw_m1911                  = "f",
    cw_m249_official          = "z",
    cw_m3super90              = "k",
    cw_mac11                  = "l",
    cw_p99                    = "a",
    cw_makarov                = "f",
    cw_shorty                 = "k",
    cw_smoke_grenade          = "Q",
    cw_vss                    = "i",
}

local WIOffsets = {
    hands              = 10,
    none               = 10,
    weapon_medkit      = 5,
    weapon_slap        = 5,
    weapon_alyxgun     = -5,
    weapon_annabelle   = 0,
    weapon_handgrenade = 10,
    cw_mr96            = 10,
    weapon_alyxgunr    = -5,
    weapon_annabeller  = 0,
    weapon_cyn_rr      = 10,
    weapon_archerxbow  = 10,
    weapon_coilgun     = 10,
    weapon_plasmanade  = 10,
    weapon_scrapper    = 25,
    oc_grapple         = -5,
}

local HL1Weps = {
    weapon_hl1_357         = true,
    weapon_hl1_glock       = true,
    weapon_hl1_crossbow    = true,
    weapon_hl1_crowbar     = true,
    weapon_hl1_egon        = true,
    weapon_hl1_handgrenade = true,
    weapon_hl1_hornetgun   = true,
    weapon_hl1_mp5         = true,
    weapon_hl1_rpg         = true,
    weapon_hl1_satchel     = true,
    weapon_hl1_shotgun     = true,
    weapon_hl1_snark       = true,
    weapon_hl1_gauss       = true,
    weapon_hl1_tripmine    = true,
}

local function DrawFontIcon(wep,x,y,w,h,tbl,font1,font2,offset)
    y = y + (WIOffsets[wep:GetClass()] and WIOffsets[wep:GetClass()] or offset)
    x = x + 10
    w = w - 20

    draw_DrawText(tbl[wep:GetClass()],font2,x+w/2,y,CHUD.Utils.GetHUDColor(),TEXT_ALIGN_CENTER)
    draw_DrawText(tbl[wep:GetClass()],font1,x+w/2,y,CHUD.Utils.GetHUDColor(),TEXT_ALIGN_CENTER)
end

local function DWSHL1( wep, x, y, wide, tall, alpha )
    local col = CHUD.Utils.GetHUDColor()
    surface_SetDrawColor( col.r, col.g, col.b, alpha )
    surface_SetTexture( wep.WepSelectIcon )

    y = y + 30
    x = x + 10
    wide = wide - 20

    surface_DrawTexturedRect( x, y, wide, wide / 2 )
    --if isfunction(wep.PrintWeaponInfo) then wep:PrintWeaponInfo( x + wide + 20, y + tall * 0.95, alpha ) end
end

local iconCache = {}
local defaultIcon = surface_GetTextureID("weapons/swep")

local function DWSFallback( wep, x, y, wide, tall, alpha )
    if not iconCache[wep:GetClass()] then
        if Material("vgui/entities/"..wep:GetClass()):IsError() then
            iconCache[wep:GetClass()] = defaultIcon
        else
            iconCache[wep:GetClass()] = surface_GetTextureID("vgui/entities/"..wep:GetClass())
        end
    end

    surface_SetDrawColor( 255, 255, 255, alpha )
    if tostring(wep.WepSelectIcon):find("Material") then
        surface_SetTexture(defaultIcon)
    else
        surface_SetTexture(wep.WepSelectIcon == defaultIcon and iconCache[wep:GetClass()] or wep.WepSelectIcon)
    end

    y = y + 10
    x = x + 10
    wide = wide - 20

    surface_DrawTexturedRect( x, y, wide, wide / 2 )
    if isfunction(wep.PrintWeaponInfo) and wep.DrawWeaponInfoBox == true then wep:PrintWeaponInfo( x + wide + 20, y + tall * 0.95, alpha ) end
end

local DWSCustom = {}

local curstyle = CHUD.CVars.StyleWeapon
local style = CHUD.Styles[curstyle:GetString()] and CHUD.Styles[curstyle:GetString()].HasWeaponSwitcher and CHUD.Styles[curstyle:GetString()] or CHUD.Styles["hl2"]
local styleOptions = style.WeaponSwitcher

local function DrawWeaponBox(x,y,wep,selected)
    if not IsValid(wep) then return end
    local w,h = ScreenScaleH(styleOptions.WeaponWidth), selected and ScreenScaleH(styleOptions.WeaponHeightSelected) or ScreenScaleH(styleOptions.WeaponHeight)
    draw_RoundedBox(10, x, y, w, h, CHUD.Utils.GetBGColor())

    local tw,th = GTSC(styleOptions.Font or "CHUD-HUDSelectionText",wep.PrintName or (WeaponNames[wep:GetClass()] or language_GetPhrase(wep:GetClass())))
    draw_DrawText(WeaponNames[wep:GetClass()] or (wep.PrintName or language_GetPhrase(wep:GetClass())), styleOptions.Font or "CHUD-HUDSelectionText", x+w/2, y+(selected and ScreenScaleH(styleOptions.TextYPos) or (styleOptions.WeaponHeight > styleOptions.TextYPos and ScreenScaleH(styleOptions.TextYPos) or ScreenScaleH(styleOptions.WeaponHeight)/2)), CHUD.Utils.GetHUDColor(), TEXT_ALIGN_CENTER)

    if wep.BounceWeaponIcon then wep.BounceWeaponIcon = false end

    local frac = 1

    if styleOptions.IconHeight and styleOptions.IconHeight ~= 64 then
        frac = ScreenScaleH(styleOptions.IconHeight)/ScreenScaleH(64)
    end

    if selected then
        local _y = y-ScreenScaleH(styleOptions.IconOffset or 0)
        --[[if frac ~= 1 then
            local sw = ((ScreenScaleH(styleOptions.BoxSize)+ScreenScaleH(styleOptions.BoxGap))*5)+ScreenScaleH(styleOptions.WeaponWidth)

            local matrix = Matrix()
            matrix:SetTranslation(Vector((CHUD.CurResW*frac)-(x*frac)+(w*frac*0.5),y+(h*frac),0))
            matrix:Scale(Vector(frac,frac,0))
            cam.PushModelMatrix(matrix)
        end--]]
        if WeaponIconsHL2[wep:GetClass()] then
            DrawFontIcon(wep,x,_y,w,h,WeaponIconsHL2,"CHUD-WeaponSelect-HL2","CHUD-WeaponSelected-HL2",24)
        elseif WeaponIconsCS[wep:GetClass()] then
            DrawFontIcon(wep,x,_y,w,h,WeaponIconsCS,"CHUD-WeaponSelect-CS","CHUD-WeaponSelected-CS",30)
        elseif WeaponIconsCSD[wep:GetClass()] then
            DrawFontIcon(wep,x,_y,w,h,WeaponIconsCSD,"CHUD-WeaponSelect-CSD","CHUD-WeaponSelected-CSD",60)
        elseif HL1Weps[wep:GetClass()] then
            DWSHL1(wep,x,_y,w,h,255)
        elseif DWSCustom[wep:GetClass()] then
            DWSCustom[wep:GetClass()](x,_y,w,h,255)
        else
            if wep.DrawWeaponSelection and isfunction(wep.DrawWeaponSelection) then
                wep:DrawWeaponSelection(x,y,w,h,255)
            else
                DWSFallback(wep,x,y,w,h,255)
            end
        end
        --[[if frac ~= 1 then
            cam.PopModelMatrix()
        end--]]
    end

    return h
end

local boxSize = ScreenScaleH(styleOptions.BoxSize)
local function DrawSlot(x,y,slot)
    if (tCacheLength and tCacheLength[slot] == 0) and not styleOptions.HideEmptySlots then
        draw_RoundedBox(10, x, y, boxSize, boxSize, CHUD.Utils.GetBGColor())
    end
    if tCacheLength and tCacheLength[slot] ~= 0 then
        draw_RoundedBox(10, x, y, boxSize, boxSize, CHUD.Utils.GetBGColor())
        draw_DrawText(slot,"CHUD-HUDSelectionNumbers",x+ScreenScaleH(styleOptions.SelectionNumberPosX),y+ScreenScaleH(styleOptions.SelectionNumberPosY),CHUD.Utils.GetHUDColor())
    end
end

local spacing = ScreenScaleH(styleOptions.BoxGap)
local w = ScreenScaleH(styleOptions.WeaponWidth)
local wbw = boxSize+spacing
local slots = {
    [1] = function(x,y)
        for i=0,4 do
            DrawSlot(x+w+spacing+((spacing+boxSize)*i), y, i+2)
        end
    end,
    [2] = function(x,y)
        DrawSlot(x,y,1)
        for i=0,3 do
            DrawSlot(x + spacing + boxSize + w + spacing + ((spacing+boxSize)*i), y, i+3)
        end
    end,
    [3] = function(x,y)
        DrawSlot(x, y, 1)
        DrawSlot(x + (spacing + boxSize), y, 2)
        for i=0,2 do
            DrawSlot(x + (spacing + boxSize)*2 + w + spacing + ((spacing+boxSize)*i), y, i+4)
        end
    end,
    [4] = function(x,y)
         for i=0,2 do
            DrawSlot(x + (spacing + boxSize)*i, y, i+1)
        end
        for i=0,1 do
            DrawSlot(x + (spacing + boxSize)*3 + w + spacing + ((spacing+boxSize)*i), y, i+5)
        end
    end,
    [5] = function(x,y)
        for i=0,3 do
            DrawSlot(x + (spacing + boxSize)*i, y, i+1)
        end
        DrawSlot(x + (spacing + boxSize)*4 + w + spacing, y, 6)
    end,
    [6] = function(x,y)
        for i=0,4 do
            DrawSlot(x + (spacing + boxSize)*i, y, i+1)
        end
    end
}

local function DrawWeaponSelector(x,y,slot,weps,pos)
    for n,wslot in pairs(weps) do
        if n == slot-1 and pos > 0 then
            local x,y = x,y
            for c,wep in pairs(wslot) do
                local h = DrawWeaponBox(x+wbw*(slot-2),y,wep,c==pos) or 0
                if c == 1 then
                    draw_DrawText(slot-1,"CHUD-HUDSelectionNumbers",x+wbw*(slot-2)+ScreenScaleH(styleOptions.SelectionNumberPosX),y+ScreenScaleH(styleOptions.SelectionNumberPosY),CHUD.Utils.GetHUDColor())
                end
                y = y+h+spacing
            end
        end
    end

    slots[slot-1](x,y)
end

local function DrawSelector()
    DrawWeaponSelector(CHUD.CurResW/2-((wbw*5)+w)/2,ScreenScaleH(16),curSlot+1,tCache,curPos)
end

cvars.AddChangeCallback("chud_style_wpn", function()
    style = CHUD.Styles[curstyle:GetString()] and CHUD.Styles[curstyle:GetString()].HasWeaponSwitcher and CHUD.Styles[curstyle:GetString()] or CHUD.Styles["hl2"]
    styleOptions = style.WeaponSwitcher

    boxSize = ScreenScaleH(styleOptions.BoxSize)
    spacing = ScreenScaleH(styleOptions.BoxGap)
    w = ScreenScaleH(styleOptions.WeaponWidth)
    wbw = ScreenScaleH(styleOptions.BoxSize)+spacing
end, "chud_style_wpn")

for i=1,6 do
    tCache[i] = {}
    tCacheLength[i] = 0
end

local function PrecacheWeapons()
    for i=1,6 do
        for j=1,tCacheLength[i] do
            tCache[i][j] = nil
        end
        tCacheLength[i] = 0
    end

    wepCount = 0
    for _,wep in pairs(lply:GetWeapons()) do
        wepCount = wepCount+1

        local slot = wep:GetSlot()+1

        if slot <= 6 then
            local len = tCacheLength[slot]+1
            tCacheLength[slot] = len
            tCache[slot][len] = wep
        end
    end

    for i=1,6 do
        table_sort(tCache[i],function(a,b) return a:GetSlotPos() < b:GetSlotPos() end)
    end

    if curSlot ~= 0 then
        local len = tCacheLength[curSlot]
        if len < curSlot then
            curSlot = len == 0 and 0 or len
        end
    end
end

local w_alpha = 1
local wpnFading = false
local enabled = CHUD.CVars.Enabled
local shouldDraw = CHUD.CVars.WeaponSelector
hook.Add("DrawOverlay","CHUD.WeaponSelector",function()
    if not IsValid(lply) then lply = LocalPlayer() end
    if not shouldDraw:GetBool() or not enabled:GetBool() then return end

    if IsValid(lply)
    and lply:Alive()
    and (wepCount < table_Count(lply:GetWeapons()) or not IsValid(tCache) or not IsValid(tCacheLength) or table_Count(tCache) == 0 or table_Count(tCacheLength) == 0)
    and curSlot == 0
    then
        PrecacheWeapons()
    end

    if curSlot == 0 then return end

    local show
    show = show or SysTime() < selTime+2
    w_alpha = Lerp(RealFrameTime() * 3.5,w_alpha,show and 1 or 0)
    if w_alpha <= 0.005 then
        curSlot = 0

        selTime = SysTime()
        w_alpha = 1
        wpnFading = false
        return
    end

    if w_alpha < 1 then
        wpnFading = true
    end

    surface.SetAlphaMultiplier(w_alpha)
    if IsValid(lply) and lply:Alive() and (not lply:InVehicle() or lply:GetAllowWeaponsInVehicle()) then
        DrawSelector()
    else
        curSlot = 0
    end
    surface.SetAlphaMultiplier(1)
end)

--Logic
local hud_fastswitch = GetConVar("hud_fastswitch")
hook.Add("PlayerBindPress","CHUD_WeaponSelector",function(ply,bind,pressed)
    if not IsValid(lply) then lply = LocalPlayer() end
    if not ply:Alive()
    or ply:InVehicle() and not ply:GetAllowWeaponsInVehicle()
    or hud_fastswitch:GetBool()
    or not shouldDraw:GetBool() or not enabled:GetBool()
    or (IsValid(ply:GetActiveWeapon()) and ply:GetActiveWeapon():GetClass() == "weapon_physgun" and physgunPickup and table.HasValue({"invprev","invnext"},bind))
    or ply ~= lply
    or (IsValid(ply:GetActiveWeapon()) and ply:GetActiveWeapon():GetClass() == "gmod_tool" and ply:GetActiveWeapon():GetTable().current_mode == "wire_adv" and table.HasValue({"invprev","invnext"},bind))
    then return end

    if input_IsKeyDown(KEY_LALT) then return end

    bind = bind:lower()

    if bind == "cancelselect" then
        if pressed then curSlot = 0 end
        return true
    end

    if bind == "invprev" then
        if not pressed then return true end

        if wepCount == 0 then return true end

        if wpnFading == true then wpnFading = false end

        local loop = curSlot == 0

        if loop then
            local wep = ply:GetActiveWeapon()
            local slot = 1
            local slotCache = tCache[slot]
            if IsValid(wep) then
                slot = wep:GetSlot()+1
                slotCache = tCache[slot]
            end

            if slotCache[1] ~= wep then
                curSlot = slot
                curPos = 1

                for i=2,tCacheLength[slot] do
                    if slotCache[i] == wep then
                        curPos = i-1
                        break
                    end
                end

                selTime = SysTime()
                ply:EmitSound(sndMove)
                w_alpha = 1

                return true
            end

            curSlot = slot
        end

        if loop or curPos == 1 then
            for i=1,6 do
                if curSlot <= 1 then
                    curSlot = 6
                else
                    curSlot = curSlot-1
                end
                if tCacheLength[curSlot] ~= 0 then break end
            end

            curPos = tCacheLength[curSlot]
        else
            curPos = curPos-1
        end

        selTime = SysTime()
        ply:EmitSound(sndMove)
        w_alpha = 1

        return true
    end

    if bind == "invnext" then
        if not pressed then return true end

        if wepCount == 0 then return true end

        if wpnFading == true then wpnFading = false end

        local loop = curSlot == 0

        if loop then
            local wep = ply:GetActiveWeapon()

            local slot = 1
            local len = tCacheLength[slot]
            local slotCache = tCache[slot]
            if IsValid(wep) then
                slot = wep:GetSlot()+1
                len = tCacheLength[slot]
                slotCache = tCache[slot]
            end

            if IsValid(wep) and slotCache[len] ~= wep then
                curSlot = slot
                curPos = 1

                for i=1,len-1 do
                    if slotCache[i] == wep then
                        curPos = i+1
                        break
                    end
                end

                selTime = SysTime()
                ply:EmitSound(sndMove)
                w_alpha = 1

                return true
            end

            curSlot = slot
        end

        if loop or curPos == tCacheLength[curSlot] then
            for i=1,6 do
                if curSlot == 6 then
                    curSlot = 1
                else
                    curSlot = curSlot+1
                end
                if tCacheLength[curSlot] ~= 0 then break end
            end

            curPos = 1
        else
            curPos = curPos+1
        end

        selTime = SysTime()
        ply:EmitSound(sndMove)
        w_alpha = 1

        return true
    end

    if bind:sub(1,4) == "slot" then
        if wpnFading == true then wpnFading = false end
        local slot = tonumber(bind:sub(5))

        if slot == nil or slot <= 0 or slot > 6 then return end
        if not pressed then return true end

        ply:EmitSound(sndMove)

        if tCacheLength[slot] <= 0 then return true end

        if wepCount == 0 then ply:EmitSound(sndMove) return true end

        if slot <= 6 then
            if curSlot > 0 then ply:EmitSound(sndMove) end

            if slot == curSlot then
                if curPos == tCacheLength[curSlot] then
                    curPos = 1
                else
                    curPos = curPos+1
                end
            elseif tCacheLength[slot] ~= 0 then
                curSlot = slot
                curPos = 1
            end

            selTime = SysTime()
            w_alpha = 1
        end

        return true
    end

    if curSlot ~= 0 then
        if bind == "+attack" then
            if wpnFading == true then return false end
            local wep = NULL
            if curPos > 0 then wep = tCache[curSlot][curPos] end

            curSlot = 0

            if IsValid(wep) and wep ~= ply:GetActiveWeapon() then
                input_SelectWeapon(wep)
            end

            selTime = SysTime()
            ply:EmitSound(sndSelect)

            return true
        end

        if bind == "+attack2" then
            selTime = SysTime()
            curSlot = 0

            return true
        end
    end
end)