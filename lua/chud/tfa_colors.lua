--hook.Remove("HUDPaint","CHUD_TFA")
do return end

local TFA_INSPECTIONPANEL = {}

hook.Add("HUDPaint","CHUD_TFA",function()
    if not TFA then
        hook.Remove("HUDPaint","CHUD_TFA")
        return
    end
    TFA.Attachments.Colors["background"] = CHUD.Utils.GetBGColor()
    TFA.Attachments.Colors["secondary"] = CHUD.Utils.GetHUDColor()
    TFA.Attachments.Colors["active"] = CHUD.Utils.GetDamageColor()
    TFA.AttachmentColors["background"] = CHUD.Utils.GetBGColor()
    TFA.AttachmentColors["secondary"] = CHUD.Utils.GetHUDColor()
    TFA.AttachmentColors["active"] = CHUD.Utils.GetDamageColor()

    if TFA_INSPECTIONPANEL and IsValid(TFA_INSPECTIONPANEL) and ispanel(TFA_INSPECTIONPANEL) then
        TFA_INSPECTIONPANEL.Paint = function(myself, w, h)
            local wep = LocalPlayer():GetActiveWeapon()

            if IsValid(wep) then
                myself.Alpha = wep.InspectingProgress * 255
                myself.PrimaryColor = ColorAlpha(TFA.Attachments.Colors["primary"], TFA_INSPECTIONPANEL.Alpha)
                myself.SecondaryColor = ColorAlpha(CHUD.Utils.GetHUDColor(), TFA_INSPECTIONPANEL.Alpha)
                myself.BackgroundColor = ColorAlpha(CHUD.Utils.GetBGColor(), TFA_INSPECTIONPANEL.Alpha)
                myself.ActiveColor = ColorAlpha(TFA.Attachments.Colors["active"], TFA_INSPECTIONPANEL.Alpha)

                if not myself.SideBar then
                    myself.SideBar = surface.GetTextureID("vgui/inspectionhud/sidebar")
                end

                if not myself.Hex then
                    myself.Hex = surface.GetTextureID("vgui/inspectionhud/hex")
                end
            end
        end
    else
        for k,v in pairs(vgui.GetWorldPanel():GetChildren()) do
            if v.Weapon and IsValid(v.Weapon) and v.Weapon:IsTFA() then
                TFA_INSPECTIONPANEL = v
                break
            end
        end
    end
end)