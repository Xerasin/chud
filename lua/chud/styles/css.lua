local draw,surface,math,string,LocalPlayer,ScreenScaleH = draw,surface,math,string,LocalPlayer,CHUD.Utils.ScreenScaleH
local GTSC = CHUD.Utils.GetTextSize

local lply = LocalPlayer()

local h_w = ScreenScaleH(80)
local function DrawHealth(x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local col = lply:Health() <= 25 and CHUD.Utils.GetDamageColor() or CHUD.Utils.GetHUDColor()

    local w1,h1 = GTSC("CHUD-CS-Icons","b")

    local w2,h2 = GTSC("CHUD-CS-Icons",math.max(100,lply:Health()))

    h_w = Lerp(0.1,h_w,ScreenScaleH(80)+(w2-GTSC("CHUD-CS-Icons","100")))

    draw.RoundedBox(10, x, y, h_w, ScreenScaleH(25), CHUD.Utils.GetBGColor())
    draw.DrawText("b","CHUD-CS-Icons",x+ScreenScaleH(8),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
    draw.DrawText(CHUD.CVars.ClampNegative:GetBool() and lply:Health() or math.max(0,lply:Health()),"CHUD-CS-Icons",x+ScreenScaleH(35),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
end

local a_w = ScreenScaleH(80)
local function DrawArmor(x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local col = CHUD.Utils.GetHUDColor()

    local w1,h1 = GTSC("CHUD-CS-Icons","a")

    local w2,h2 = GTSC("CHUD-CS-Icons",math.max(100,lply:Armor()))

    a_w = Lerp(0.1,a_w,ScreenScaleH(80)+(w2-GTSC("CHUD-CS-Icons","100")))

    draw.RoundedBox(10, x, y, a_w, ScreenScaleH(25), CHUD.Utils.GetBGColor())
    draw.DrawText("a","CHUD-CS-Icons",x+ScreenScaleH(8),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
    draw.DrawText(lply:Armor(),"CHUD-CS-Icons",x+ScreenScaleH(34),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
end

local am_w = ScreenScaleH(142)
local function DrawAmmo(x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local wep = lply:GetActiveWeapon()

    local col = (wep:Clip1() == -1 and lply:GetAmmoCount(wep:GetPrimaryAmmoType()) or wep:Clip1()) == 0 and CHUD.Utils.GetDamageColor() or CHUD.Utils.GetHUDColor()

    local w1,h1 = GTSC("CHUD-CS-Icons","0000")
    local w2,h2 = GTSC("CHUD-CS-Icons"," | ")

    draw.RoundedBox(10, x, y, am_w, ScreenScaleH(25), CHUD.Utils.GetBGColor())
    draw.DrawText(wep:Clip1(),"CHUD-CS-Icons",x+ScreenScaleH(8),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
    if wep:GetPrimaryAmmoType() ~= -1 and wep:Clip1() ~= -1 then
        draw.DrawText(lply:GetAmmoCount(wep:GetPrimaryAmmoType()),"CHUD-CS-Icons",x+ScreenScaleH(8)+w1+w2,y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
        draw.DrawText(" | ","CHUD-CS-Icons",x+ScreenScaleH(8)+w1,y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
    end
end

local function DrawAmmoCustom(x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local wep = lply:GetActiveWeapon()
    local tbl = wep:CustomAmmoDisplay()
    if not tbl or (tbl and not tbl.Draw) then return end
    if not tbl.PrimaryAmmo and not tbl.PrimaryClip then return end

    local col = (tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0)) == 0 and CHUD.Utils.GetDamageColor() or CHUD.Utils.GetHUDColor()

    local w1,h1 = GTSC("CHUD-CS-Icons","0000")
    local w2,h2 = GTSC("CHUD-CS-Icons"," | ")

    draw.RoundedBox(10, x, y, am_w, ScreenScaleH(25), CHUD.Utils.GetBGColor())
    draw.DrawText(tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0),"CHUD-CS-Icons",x+ScreenScaleH(8),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
    if tbl.PrimaryAmmo and tbl.PrimaryClip then
        draw.DrawText(tbl.PrimaryAmmo,"CHUD-CS-Icons",x+ScreenScaleH(8)+w1+w2,y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
        draw.DrawText(" | ","CHUD-CS-Icons",x+ScreenScaleH(8)+w1,y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
    end
end

local am2_w = ScreenScaleH(80)
local function DrawAmmoAlt(x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local col = CHUD.Utils.GetHUDColor()

    local wep = lply:GetActiveWeapon()

    local w1,h1 = GTSC("CHUD-CS-Icons",lply:GetAmmoCount(wep:GetSecondaryAmmoType()))

    draw.RoundedBox(10, x, y, am2_w, ScreenScaleH(25), CHUD.Utils.GetBGColor())
    draw.DrawText(lply:GetAmmoCount(wep:GetSecondaryAmmoType()),"CHUD-CS-Icons",x+ScreenScaleH(8),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
end

local function DrawAmmoAltCustom(x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local col = CHUD.Utils.GetHUDColor()

    local wep = lply:GetActiveWeapon()

    local tbl = wep:CustomAmmoDisplay()
    if not tbl or (tbl and not tbl.Draw) then return end
    if not tbl.SecondaryAmmo then return end

    draw.RoundedBox(10, x, y, am2_w, ScreenScaleH(25), CHUD.Utils.GetBGColor())
    draw.DrawText(tbl.SecondaryAmmo or 0,"CHUD-CS-Icons",x+ScreenScaleH(8),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
end

local function GetPlayerVelocity()
    if not IsValid(lply) then lply = LocalPlayer() end
    local v = IsValid(lply:GetVehicle()) and (IsValid(lply:GetVehicle():GetParent()) and lply:GetVehicle():GetParent():GetVelocity() or lply:GetVehicle():GetVelocity()) or lply:GetVelocity()

    return v
end

local v_w = ScreenScaleH(98)
local function DrawVelocity(x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local col = CHUD.Utils.GetHUDColor()

    local w1,h1 = GTSC("CHUD-CS-Icons","»")

    local v = math.Round(GetPlayerVelocity():Length())

    local w2,h2 = GTSC("CHUD-CS-Icons",math.max(100,v))

    draw.RoundedBox(10, x, y, v_w, ScreenScaleH(25), CHUD.Utils.GetBGColor())
    draw.DrawText("»","CHUD-CS-Icons",x+ScreenScaleH(8),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
    draw.DrawText(v,"CHUD-CS-Icons",x+ScreenScaleH(34),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
end

local am2_pos = ScreenScaleH(394)
local dh_w = ScreenScaleH(80)

local function Draw()
    if not IsValid(lply) then lply = LocalPlayer() end
    if CHUD.Utils.ShouldDrawHealth() then
        DrawHealth(ScreenScaleH(8),ScreenScaleH(446))
    end

    if CHUD.Utils.ShouldDrawArmor() then
        DrawArmor(ScreenScaleH(148),ScreenScaleH(446))
    end

    if IsValid(lply:GetActiveWeapon()) then
        if lply:GetActiveWeapon().CustomAmmoDisplay and lply:GetActiveWeapon():CustomAmmoDisplay() ~= nil then
            DrawAmmoCustom(CHUD.CurResW-ScreenScaleH(157),ScreenScaleH(446))
        elseif lply:GetActiveWeapon():GetPrimaryAmmoType() ~= -1 then
            DrawAmmo(CHUD.CurResW-ScreenScaleH(157),ScreenScaleH(446))
        end

        if lply:GetActiveWeapon().CustomAmmoDisplay and lply:GetActiveWeapon():CustomAmmoDisplay() ~= nil and lply:GetActiveWeapon():CustomAmmoDisplay().SecondaryAmmo then
            local tbl = lply:GetActiveWeapon():CustomAmmoDisplay()
            am2_pos = Lerp(0.075,am2_pos,(tbl.PrimaryClip == nil and tbl.PrimaryAmmo == nil) and ScreenScaleH(446) or ScreenScaleH(394))
            DrawAmmoAltCustom(CHUD.CurResW-ScreenScaleH(95),am2_pos)
        elseif lply:GetActiveWeapon():GetSecondaryAmmoType() ~= -1 then
            am2_pos = Lerp(0.075,am2_pos,lply:GetActiveWeapon():GetPrimaryAmmoType() == -1 and ScreenScaleH(446) or ScreenScaleH(394))
            DrawAmmoAlt(CHUD.CurResW-ScreenScaleH(95),am2_pos)
        end
    end

    if CHUD.CVars.ShowVelocity:GetBool() then
        DrawVelocity((CHUD.CurResW/2)-(v_w/2),ScreenScaleH(446))
    end
end

local function Demo(amt,x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local col = amt <= 25 and CHUD.Utils.GetDamageColor() or CHUD.Utils.GetHUDColor()

    local w1,h1 = GTSC("CHUD-CS-Icons","b")

    local w2,h2 = GTSC("CHUD-CS-Icons",math.max(100,amt))

    dh_w = Lerp(0.1,dh_w,ScreenScaleH(80)+(w2-GTSC("CHUD-CS-Icons","100")))

    draw.RoundedBox(10, x, y, dh_w, ScreenScaleH(25), CHUD.Utils.GetBGColor())
    draw.DrawText("b","CHUD-CS-Icons",x+ScreenScaleH(8),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
    draw.DrawText(CHUD.CVars.ClampNegative:GetBool() and amt or math.max(0,amt),"CHUD-CS-Icons",x+ScreenScaleH(35),y+ScreenScaleH(-4),col,TEXT_ALIGN_LEFT)
end

CHUD.Styles["css"] = {
    Name = "Counter-Strike: Source",
    Draw = Draw,
    Demo = Demo
}