local LocalPlayer = LocalPlayer
local IsValid = IsValid
local Lerp = Lerp
local RealFrameTime = RealFrameTime
local ColorAlpha = ColorAlpha

local draw_RoundedBox = draw.RoundedBox
local draw_DrawText = draw.DrawText

local game_GetAmmoName = game.GetAmmoName

local math_floor = math.floor
local math_max = math.max
local math_Round = math.Round

local ScreenScaleH = CHUD.Utils.ScreenScaleH
local GTSC = CHUD.Utils.GetTextSize
local IntersectColor = CHUD.Utils.IntersectColor
local GetHUDColor = CHUD.Utils.GetHUDColor
local GetDamageColor = CHUD.Utils.GetDamageColor
local GetBGColorNoAlpha = CHUD.Utils.GetBGColorNoAlpha
local GetBGColor = CHUD.Utils.GetBGColor

local clampNegative = CHUD.CVars.ClampNegative
local backgroundAlpha = CHUD.CVars.BackgroundAlpha
local ammoIcons = CHUD.CVars.AmmoIcons
local ammoIconsSecondary = CHUD.CVars.AmmoIconsSecondary
local altAmmoOldPos = CHUD.CVars.AltAmmoOldPos
local showVelocity = CHUD.CVars.ShowVelocity
local velocityAltPos = CHUD.CVars.VelocityAltPos

local lply = LocalPlayer()
local oldwep = IsValid(lply) and IsValid(lply:GetActiveWeapon()) and lply:GetActiveWeapon() or NULL

local ammoChars = {
    ["357"] = "q",
    ["alyxgun"] = "p",
    ["ar2"] = "u",
    ["ar2altfire"] = "z",
    ["xbowbolt"] = "w",
    ["grenade"] = "v",
    ["pistol"] = "p",
    ["buckshot"] = "s",
    ["rpg_round"] = "x",
    ["smg1"] = "r",
    ["smg1_grenade"] = "t",
}

local ammoCharsWeps = {
    ["weapon_annabelle"] = "s",
    ["weapon_annabeller"] = "s"
}

local h_old = 0
local h_glowa = 255
local h_flash = 1
local h_w = ScreenScaleH(102)
local h_c = 0
local function DrawHealth(x, y)
    if not IsValid(lply) then lply = LocalPlayer() end

    local c = 1
    if lply:Health() / lply:GetMaxHealth() <= 0.25 then c = 0 end
    h_c = Lerp(RealFrameTime() * 4, h_c, c)

    if lply:Health() / lply:GetMaxHealth() <= 0.25 then
        h_flash = Lerp(RealFrameTime() / 0.8, h_flash, 0)
        if math_floor(h_flash + 0.5) == 0 then
            h_flash = 1
        end
    else
        h_flash = Lerp(RealFrameTime() * 4, h_flash, 0)
    end

    local col = IntersectColor(GetHUDColor(), GetDamageColor(), h_c)
    local bgcol = ColorAlpha(IntersectColor(GetDamageColor(), GetBGColorNoAlpha(), h_flash), backgroundAlpha:GetInt())

    local w1, h1 = GTSC("CHUD-HL2-HUDDefault", "HEALTH")

    if lply:Health() ~= h_old then
        h_old = lply:Health()
        h_glowa = 255
    end

    h_glowa = Lerp(RealFrameTime(), h_glowa, 0)

    local w2, h2 = GTSC("CHUD-HL2-HUDNumbers", math_max(100, lply:Health()))

    h_w = Lerp(RealFrameTime() / 0.1, h_w, ScreenScaleH(102) + (w2 - GTSC("CHUD-HL2-HUDNumbers", "100")))

    draw_RoundedBox(10, x, y, h_w, ScreenScaleH(36), bgcol)
    draw_DrawText("HEALTH", "CHUD-HL2-HUDDefault",x + ScreenScaleH(8), y + ScreenScaleH(20), col, TEXT_ALIGN_LEFT)
    draw_DrawText(clampNegative:GetBool() and lply:Health() or math_max(0, lply:Health()), "CHUD-HL2-HUDNumbersGlow", x+ScreenScaleH(50), y+ScreenScaleH(2), ColorAlpha(col, h_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(clampNegative:GetBool() and lply:Health() or math_max(0, lply:Health()), "CHUD-HL2-HUDNumbers", x+ScreenScaleH(50), y+ScreenScaleH(2), col, TEXT_ALIGN_LEFT)
end

local a_old = 0
local a_glowa = 255
local a_w = ScreenScaleH(108)
local function DrawArmor(x, y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local col = GetHUDColor()

    local w1, h1 = GTSC("CHUD-HL2-HUDDefault", "SUIT")

    if lply:Armor() ~= a_old then
        a_old = lply:Armor()
        a_glowa = 255
    end

    a_glowa = Lerp(RealFrameTime(), a_glowa, 0)

    local w2, h2 = GTSC("CHUD-HL2-HUDNumbers", math_max(100, lply:Armor()))

    a_w = Lerp(RealFrameTime()/0.1, a_w, ScreenScaleH(108) + (w2 - GTSC("CHUD-HL2-HUDNumbers", "100")))

    draw_RoundedBox(10, x, y, a_w, ScreenScaleH(36), GetBGColor())
    draw_DrawText("SUIT", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20), col, TEXT_ALIGN_LEFT)
    draw_DrawText(lply:Armor(), "CHUD-HL2-HUDNumbersGlow",x + ScreenScaleH(50), y + ScreenScaleH(2), ColorAlpha(col, a_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(lply:Armor(), "CHUD-HL2-HUDNumbers", x + ScreenScaleH(50), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)
end

local am_old = 0
local am_glowa = 255
local am_bga = backgroundAlpha:GetInt()
local am_w = ScreenScaleH(132)
local am_flash = 0
local am_c = 0
local function DrawAmmo(x, y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local wep = lply:GetActiveWeapon()

    if IsValid(lply:GetVehicle()) and not lply:GetAllowWeaponsInVehicle() then return end

    local c = 1
    if (wep:Clip1() == -1 and lply:GetAmmoCount(wep:GetPrimaryAmmoType()) or wep:Clip1()) == 0 then c = 0 end
    am_c = Lerp(RealFrameTime() * 4, am_c, c)

    if (wep:Clip1() == -1 and lply:GetAmmoCount(wep:GetPrimaryAmmoType()) or wep:Clip1()) == 0 then
        am_flash = Lerp(RealFrameTime() / 0.8, am_flash, 0)
        if math_floor(am_flash + 0.5) == 0 then
            am_flash = 1
        end
    else
        am_flash = Lerp(RealFrameTime() * 4, am_flash, 0)
    end

    local col = IntersectColor(GetHUDColor(), GetDamageColor(), am_c)
    local bgcol = ColorAlpha(IntersectColor(GetDamageColor(), GetBGColorNoAlpha(), am_flash), backgroundAlpha:GetInt())

    local w1, h1 = GTSC("CHUD-HL2-HUDDefault", "AMMO")
    local w2, h2 = GTSC("CHUD-HL2-HUDNumbers", "0000")
    local w3, h3 = GTSC("CHUD-HL2-HUDNumbersSmall", lply:GetAmmoCount(wep:GetPrimaryAmmoType()))

    if (wep:Clip1() == -1 and lply:GetAmmoCount(wep:GetPrimaryAmmoType()) or wep:Clip1()) == 0 then
        am_glowa = 255
    end

    if (wep:Clip1() == -1 and lply:GetAmmoCount(wep:GetPrimaryAmmoType()) or wep:Clip1()) ~= am_old then
        am_old = wep:Clip1() == -1 and lply:GetAmmoCount(wep:GetPrimaryAmmoType()) or wep:Clip1()
        am_glowa = 255
    end

    am_glowa = Lerp(RealFrameTime(), am_glowa, 0)
    am_w = Lerp(RealFrameTime() / 0.1, am_w, (wep:GetPrimaryAmmoType() == -1 or wep:Clip1() == -1) and ScreenScaleH(16) + w1 + w2 + (lply:GetAmmoCount(wep:GetPrimaryAmmoType()) > 1000 and ScreenScaleH(8) or 0) or ScreenScaleH(132))

    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), bgcol)
    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), ColorAlpha(col, am_bga))
    draw_DrawText("AMMO", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20), col, TEXT_ALIGN_LEFT)
    if (ammoCharsWeps[wep:GetClass()] or ammoChars[game_GetAmmoName(wep:GetPrimaryAmmoType()):lower()]) and ammoIcons:GetBool() then
        local nLabelWidth, nLabelHeight = GTSC("CHUD-HL2-HUDDefault", "AMMO")
        local iw, ih = GTSC("CHUD-HL2-WeaponIconsSmall", ammoCharsWeps[wep:GetClass()] or ammoChars[game_GetAmmoName(wep:GetPrimaryAmmoType()):lower()] or "")
        local ix = ScreenScaleH(8) + (nLabelWidth - iw) / 2
        local iy = ScreenScaleH(20) - (nLabelHeight + (ih / 2))

        draw_DrawText(ammoCharsWeps[wep:GetClass()] or ammoChars[game_GetAmmoName(wep:GetPrimaryAmmoType()):lower()] or "", "CHUD-HL2-WeaponIconsSmall", x + ix, y + iy, col, TEXT_ALIGN_LEFT)
    end
    draw_DrawText(wep:Clip1() == -1 and lply:GetAmmoCount(wep:GetPrimaryAmmoType()) or wep:Clip1(), "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(44), y + ScreenScaleH(2), ColorAlpha(col, am_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(wep:Clip1() == -1 and lply:GetAmmoCount(wep:GetPrimaryAmmoType()) or wep:Clip1(), "CHUD-HL2-HUDNumbers", x + ScreenScaleH(44), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)
    if wep:GetPrimaryAmmoType() ~= -1 and wep:Clip1() ~= -1 then
        draw_DrawText(lply:GetAmmoCount(wep:GetPrimaryAmmoType()), "CHUD-HL2-HUDNumbersSmall", x + ScreenScaleH(98), y + ScreenScaleH(16), col, TEXT_ALIGN_LEFT)
    end
end

local function DrawAmmoCustom(x, y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local wep = lply:GetActiveWeapon()

    if IsValid(lply:GetVehicle()) and not lply:GetAllowWeaponsInVehicle() then return end

    local tbl = wep:CustomAmmoDisplay()
    if not tbl or (tbl and not tbl.Draw) then return end
    if not tbl.PrimaryAmmo and not tbl.PrimaryClip then return end

    local c = 1
    if (tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0)) == 0 then c = 0 end
    am_c = Lerp(RealFrameTime() * 4, am_c, c)

    if (tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0)) == 0 then
        am_flash = Lerp(RealFrameTime() / 0.8, am_flash, 0)
        if math_floor(am_flash + 0.5) == 0 then
            am_flash = 1
        end
    else
        am_flash = Lerp(RealFrameTime() * 4, am_flash, 0)
    end

    local col = IntersectColor(GetHUDColor(), GetDamageColor(), am_c)
    local bgcol = ColorAlpha(IntersectColor(GetDamageColor(), GetBGColorNoAlpha(), am_flash), backgroundAlpha:GetInt())

    local w1, h1 = GTSC("CHUD-HL2-HUDDefault", "AMMO")
    local w2, h2 = GTSC("CHUD-HL2-HUDNumbers", "0000")
    local w3, h3 = GTSC("CHUD-HL2-HUDNumbersSmall", tbl.PrimaryAmmo or 0)

    if (tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0)) == 0 then
        am_glowa = 255
    end

    if (tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0)) ~= am_old then
        am_old = tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0)
        am_glowa = 255
    end

    am_glowa = Lerp(RealFrameTime(), am_glowa, 0)
    am_w = Lerp(RealFrameTime() / 0.1, am_w, (not tbl.PrimaryAmmo or not tbl.PrimaryClip) and ScreenScaleH(24) + w1 + w2 or ScreenScaleH(136))

    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), bgcol)
    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), ColorAlpha(col, am_bga))
    draw_DrawText("AMMO", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20), col, TEXT_ALIGN_LEFT)
    draw_DrawText(tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0), "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(44), y + ScreenScaleH(2), ColorAlpha(col, am_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0), "CHUD-HL2-HUDNumbers", x + ScreenScaleH(44), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)
    if tbl.PrimaryAmmo and tbl.PrimaryClip then
        draw_DrawText(tbl.PrimaryAmmo or 0, "CHUD-HL2-HUDNumbersSmall", x + ScreenScaleH(98), y + ScreenScaleH(16), col, TEXT_ALIGN_LEFT)
    end
end

local am2_old = 0
local am2_glowa = 255
local am2_w = ScreenScaleH(46)
local am2_c = 0
local function DrawAmmoAlt(x, y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local wep = lply:GetActiveWeapon()

    if IsValid(lply:GetVehicle()) and not lply:GetAllowWeaponsInVehicle() then return end

    local c = 1
    if lply:GetAmmoCount(wep:GetSecondaryAmmoType()) == 0 then c = 0 end
    am2_c = Lerp(RealFrameTime() * 4, am2_c, c)
    local col = IntersectColor(GetHUDColor(), GetDamageColor(), am2_c)

    local w1, h1 = GTSC("CHUD-HL2-HUDDefault", "ALT")
    local w2, h2 = GTSC("CHUD-HL2-HUDNumbers", lply:GetAmmoCount(wep:GetSecondaryAmmoType()))

    if lply:GetAmmoCount(wep:GetSecondaryAmmoType()) ~= am2_old then
        am2_old = lply:GetAmmoCount(wep:GetSecondaryAmmoType())
        am2_glowa = 255
    end

    am2_glowa = Lerp(RealFrameTime(), am2_glowa, 0)
    am2_w = Lerp(RealFrameTime() / 0.1, am2_w, ScreenScaleH(46) + w2)

    draw_RoundedBox(10, x, y, am2_w, ScreenScaleH(36), GetBGColor())
    draw_RoundedBox(10, x, y, am2_w, ScreenScaleH(36), ColorAlpha(col, am_bga))
    draw_DrawText("ALT", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(22), col, TEXT_ALIGN_LEFT)
    if ammoChars[game_GetAmmoName(wep:GetSecondaryAmmoType()):lower()] and ammoIconsSecondary:GetBool() then
        local nLabelWidth, nLabelHeight = GTSC("CHUD-HL2-HUDDefault", "ALT")
        local iw, ih = GTSC("CHUD-HL2-WeaponIconsSmall", ammoChars[game_GetAmmoName(wep:GetSecondaryAmmoType()):lower()] or "")
        local ix = ScreenScaleH(8) + (nLabelWidth - iw) / 2
        local iy = ScreenScaleH(22) - (nLabelHeight + (ih / 2))

        draw_DrawText(ammoChars[game_GetAmmoName(wep:GetSecondaryAmmoType()):lower()] or "", "CHUD-HL2-WeaponIconsSmall", x + ix, y + iy, col, TEXT_ALIGN_LEFT)
    end
    draw_DrawText(lply:GetAmmoCount(wep:GetSecondaryAmmoType()), "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(36), y + ScreenScaleH(2), ColorAlpha(col, am2_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(lply:GetAmmoCount(wep:GetSecondaryAmmoType()), "CHUD-HL2-HUDNumbers", x + ScreenScaleH(36), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)
end

local function DrawAmmoAltCustom(x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local wep = lply:GetActiveWeapon()

    if IsValid(lply:GetVehicle()) and not lply:GetAllowWeaponsInVehicle() then return end

    local tbl = wep:CustomAmmoDisplay()
    if not tbl or (tbl and not tbl.Draw) then return end
    if not tbl.SecondaryAmmo then return end

    local c = 1
    if (tbl.SecondaryAmmo or 0) == 0 then c = 0 end
    am2_c = Lerp(RealFrameTime() * 4, am2_c, c)
    local col = IntersectColor(GetHUDColor(), GetDamageColor(), am2_c)

    local w1, h1 = GTSC("CHUD-HL2-HUDDefault", "ALT")
    local w2, h2 = GTSC("CHUD-HL2-HUDNumbers", tbl.SecondaryAmmo or 0)

    if (tbl.SecondaryAmmo or 0) ~= am2_old then
        am2_old = tbl.SecondaryAmmo or 0
        am2_glowa = 255
    end

    am2_glowa = Lerp(RealFrameTime(), am2_glowa, 0)
    am2_w = Lerp(RealFrameTime() / 0.1, am2_w, ScreenScaleH(48) + w2)

    draw_RoundedBox(10, x, y, am2_w, ScreenScaleH(36), GetBGColor())
    draw_RoundedBox(10, x, y, am2_w, ScreenScaleH(36), ColorAlpha(col,am_bga))
    draw_DrawText("ALT", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(22), col, TEXT_ALIGN_LEFT)
    draw_DrawText(tbl.SecondaryAmmo or 0, "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(36), y + ScreenScaleH(2), ColorAlpha(col, am2_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(tbl.SecondaryAmmo or 0, "CHUD-HL2-HUDNumbers", x + ScreenScaleH(36), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)
end

local function DrawVehicleAmmo(x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local veh = lply:GetVehicle()
    local atype, clip, ammo = veh:GetAmmo()

    if ammo == -1 then return end

    local c = 1
    if (clip == -1 and ammo or clip) == 0 then c = 0 end
    am_c = Lerp(RealFrameTime() * 4, am_c, c)

    if (clip == -1 and ammo or clip) == 0 then
        am_flash = Lerp(RealFrameTime / 0.8, am_flash, 0)
        if math_floor(am_flash + 0.5) == 0 then
            am_flash = 1
        end
    else
        am_flash = Lerp(RealFrameTime() * 4, am_flash, 0)
    end

    local col = IntersectColor(GetHUDColor(), GetDamageColor(), am_c)
    local bgcol = ColorAlpha(IntersectColor(GetDamageColor(), GetBGColorNoAlpha(), am_flash), backgroundAlpha:GetInt())

    local w1, h1 = GTSC("CHUD-HL2-HUDDefault", "AMMO")
    local w2, h2 = GTSC("CHUD-HL2-HUDNumbers", "0000")
    local w3, h3 = GTSC("CHUD-HL2-HUDNumbersSmall", ammo or 0)

    if (clip == -1 and ammo or clip) == 0 then
        am_glowa = 255
    end

    if (clip == -1 and ammo or clip) ~= am_old then
        am_old = clip == -1 and ammo or clip
        am_glowa = 255
    end

    am_glowa = Lerp(RealFrameTime(), am_glowa, 0)
    am_w = Lerp(RealFrameTime() / 0.1, am_w, clip == -1 and ScreenScaleH(24) + w1 + w2 or ScreenScaleH(136))

    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), bgcol)
    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), ColorAlpha(col, am_bga))
    draw_DrawText("AMMO", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20), col, TEXT_ALIGN_LEFT)
    draw_DrawText(clip == -1 and ammo or clip, "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(44), y + ScreenScaleH(2), ColorAlpha(col, am_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(clip == -1 and ammo or clip, "CHUD-HL2-HUDNumbers", x + ScreenScaleH(44), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)
    if clip ~= -1 then
        draw_DrawText(ammo, "CHUD-HL2-HUDNumbersSmall", x + ScreenScaleH(98), y + ScreenScaleH(16), col, TEXT_ALIGN_LEFT)
    end
end

local function GetPlayerVelocity()
    if not IsValid(lply) then lply = LocalPlayer() end
    local v = IsValid(lply:GetVehicle()) and (IsValid(lply:GetVehicle():GetParent()) and lply:GetVehicle():GetParent():GetVelocity() or lply:GetVehicle():GetVelocity()) or lply:GetVelocity()

    return v
end

local v_w = ScreenScaleH(102)
local function DrawVelocity(x,y)
    if not IsValid(lply) then lply = LocalPlayer() end
    local col = GetHUDColor()

    local w1, h1 = GTSC("CHUD-HL2-HUDDefault", "UPS")

    local v = math_Round(GetPlayerVelocity():Length())

    local w2, h2 = GTSC("CHUD-HL2-HUDNumbers", math_max(100, v))

    local vw = ScreenScaleH(102) + w2 - GTSC("CHUD-HL2-HUDNumbers", "100") - GTSC("CHUD-HL2-HUDNumbers", "0")
    v_w = Lerp(RealFrameTime() / 0.1, v_w, math_max(ScreenScaleH(102), vw))

    draw_RoundedBox(10, x, y, v_w, ScreenScaleH(36), GetBGColor())
    draw_DrawText("UPS", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20), col, TEXT_ALIGN_LEFT)
    draw_DrawText(v, "CHUD-HL2-HUDNumbers", x + v_w - ScreenScaleH(8), y + ScreenScaleH(2), col, TEXT_ALIGN_RIGHT)
end

local am_pos = CHUD.CurResW - am_w
local am2_pos = ScreenScaleH(432)
local vel_xpos = ScreenScaleH(16)
local vel_ypos = ScreenScaleH(432)
local a_xpos = ScreenScaleH(16)

local function Draw()
    if not IsValid(lply) then lply = LocalPlayer() end
    local wep = lply:GetActiveWeapon()

    if wep ~= oldwep then
        oldwep = wep
        am_bga = backgroundAlpha:GetInt()
    end

    am_bga = Lerp(RealFrameTime() / 0.4, am_bga, 0)

    if CHUD.Utils.ShouldDrawHealth() then
        DrawHealth(ScreenScaleH(16), ScreenScaleH(432))
    end

    if CHUD.Utils.ShouldDrawArmor() then
        a_xpos = Lerp(RealFrameTime() / 0.4, a_xpos, ScreenScaleH(16) + (CHUD.Utils.ShouldDrawHealth() and h_w + ScreenScaleH(22) or 0))
        DrawArmor(a_xpos, ScreenScaleH(432))
    end

    if IsValid(lply:GetActiveWeapon()) then
        if lply:GetActiveWeapon().CustomAmmoDisplay and lply:GetActiveWeapon():CustomAmmoDisplay() ~= nil then
            local tbl = lply:GetActiveWeapon():CustomAmmoDisplay()
            am_pos = Lerp(RealFrameTime() / 0.4, am_pos, CHUD.CurResW - am_w - ScreenScaleH(18) - (tbl.SecondaryAmmo and (altAmmoOldPos:GetBool() and am2_w + ScreenScaleH(12) or -ScreenScaleH(2)) or 0))
            DrawAmmoCustom(am_pos, ScreenScaleH(432))
        elseif lply:GetActiveWeapon():GetPrimaryAmmoType() ~= -1 then
            am_pos = Lerp(RealFrameTime() / 0.4, am_pos, CHUD.CurResW - am_w - ScreenScaleH(18) - (lply:GetActiveWeapon():GetSecondaryAmmoType() ~= -1 and (altAmmoOldPos:GetBool() and am2_w + ScreenScaleH(12) or -ScreenScaleH(2)) or 0))
            DrawAmmo(am_pos, ScreenScaleH(432))
        end

        if lply:GetActiveWeapon().CustomAmmoDisplay and lply:GetActiveWeapon():CustomAmmoDisplay() ~= nil and lply:GetActiveWeapon():CustomAmmoDisplay().SecondaryAmmo then
            local tbl = lply:GetActiveWeapon():CustomAmmoDisplay()
            am2_pos = Lerp(RealFrameTime() / 0.4, am2_pos, (tbl.PrimaryClip == nil and tbl.PrimaryAmmo == nil) and ScreenScaleH(432) or (altAmmoOldPos:GetBool() and ScreenScaleH(432) or ScreenScaleH(386)))
            DrawAmmoAltCustom(CHUD.CurResW - am2_w - ScreenScaleH(16), am2_pos)
        elseif lply:GetActiveWeapon():GetSecondaryAmmoType() ~= -1 then
            am2_pos = Lerp(RealFrameTime() / 0.4, am2_pos, lply:GetActiveWeapon():GetPrimaryAmmoType() == -1 and ScreenScaleH(432) or (altAmmoOldPos:GetBool() and ScreenScaleH(432) or ScreenScaleH(386)))
            DrawAmmoAlt(CHUD.CurResW - am2_w - ScreenScaleH(16), am2_pos)
        end
    end

    if IsValid(lply:GetVehicle()) then
        DrawVehicleAmmo(CHUD.CurResW - am_w - ScreenScaleH(18), ScreenScaleH(432))
    end

    if showVelocity:GetBool() then
        vel_ypos = Lerp(RealFrameTime()/0.4, vel_ypos, ((AUX and AUXPOW and lply:GetInfoNum("cl_infinite_aux_power",1) == 0) or velocityAltPos:GetInt() > 0) and ScreenScaleH(432) or (CHUD.Utils.ShouldDrawHealth() and ScreenScaleH(386) or ScreenScaleH(432)))
        vel_xpos = Lerp(RealFrameTime()/0.4, vel_xpos, ((AUX and AUXPOW and lply:GetInfoNum("cl_infinite_aux_power",1) == 0) or velocityAltPos:GetInt() == 1) and ScreenScaleH(16) + (CHUD.Utils.ShouldDrawHealth() and h_w + ScreenScaleH(22) or 0) + (CHUD.Utils.ShouldDrawArmor() and a_w + ScreenScaleH(22) or 0) or (velocityAltPos:GetInt() > 1 and (CHUD.CurResW / 2) - (v_w / 2) or ScreenScaleH(16)))

        DrawVelocity(vel_xpos, vel_ypos)
    end
end

local dh_old = 0
local dh_glowa = 255
local dh_flash = backgroundAlpha:GetInt()
local dh_w = ScreenScaleH(102)
local function Demo(amt,x,y)
    local col = amt <= 25 and GetDamageColor() or GetHUDColor()
    local bgcol = GetBGColor()

    local w1, h1 = GTSC("CHUD-HL2-HUDDefault", "HEALTH")

    if amt ~= dh_old then
        dh_old = amt
        dh_glowa = 255
    end

    dh_glowa = Lerp(RealFrameTime(), dh_glowa, 0)

    if amt <= 25 then
        if math_Round(dh_flash) == 0 then dh_flash = backgroundAlpha:GetInt() dh_glowa = 255 end
        dh_flash = Lerp(RealFrameTime() / 0.4, dh_flash, 0)
        dh_glowa = 255
    else
        dh_flash = 0
    end
    local w2, h2 = GTSC("CHUD-HL2-HUDNumbers", math_max(100, amt))

    dh_w = Lerp(RealFrameTime() / 0.1, dh_w, ScreenScaleH(102) + (w2 - GTSC("CHUD-HL2-HUDNumbers", "100")))

    draw_RoundedBox(10, x, y, dh_w, ScreenScaleH(36), bgcol)
    if amt <= 25 then
        draw_RoundedBox(10, x, y, dh_w, ScreenScaleH(36), ColorAlpha(GetDamageColor(), dh_flash))
    end
    draw_DrawText("HEALTH", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20), col, TEXT_ALIGN_LEFT)
    draw_DrawText(clampNegative:GetBool() and amt or math_max(0, amt), "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(50), y + ScreenScaleH(2), ColorAlpha(col, dh_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(clampNegative:GetBool() and amt or math_max(0, amt), "CHUD-HL2-HUDNumbers", x + ScreenScaleH(50), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)
end

CHUD.Styles["hl2"] = {
    Name = "Half-Life 2",
    Draw = Draw,
    Demo = Demo,
    HasWeaponSwitcher = true,
    WeaponSwitcher = {
        WeaponWidth = 112,
        WeaponHeight = 20,
        WeaponHeightSelected = 80,
        TextYPos = 70,
        BoxGap = 8,
        BoxSize = 32,
        SelectionNumberPosX = 4,
        SelectionNumberPosY = 4,
    }
}