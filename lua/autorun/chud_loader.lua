if SERVER then
    for _,f in pairs(file.Find("chud/*","LUA")) do
        AddCSLuaFile("chud/"..f)
    end
    for _,f in pairs(file.Find("chud/styles/*","LUA")) do
        AddCSLuaFile("chud/styles/"..f)
    end
end

if CLIENT and engine.ActiveGamemode() == "sandbox" then
    include("chud/chud.lua")
end